<?php
class Site_Common extends Systems
{	
	public function test()
	{
		//echo 'masuk test';
	}
	
	public function comingsoon()
	{
		$addhtmlhead = array(
							   "meta"=>array(
							   				 array("name"=>"description","content"=>"Coming Soon...")
											 ),
							   "style"=>array(
											  ".comingsoon{background-color:rgba(0,0,0,0.9);border-radius:5px;color:white;font-size:50px;margin:180px 0px 0px 0px;width:100%;text-align:center;}"
											  )
							 	);
		$this->addhtmlhead($addhtmlhead);
		$html  = '<div class="comingsoon">';
		$html .= '	Coming Soon';
		$html .= '</div>';
		return $html;
	}
}
?>