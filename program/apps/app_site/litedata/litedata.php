<?php
$uprgm_config = array(
					  'setting'=>'some value 1',
					  'autoload'=>array(
										array('file'=>'class/site_common.php')
										)
				);
$iprgm_config = array(
					  'setting'=>'some value 2',
					  'autoload'=>array(
										//array('file'=>'class/site_commons.php')
										)
				);
$html_head = json_encode(
							array(
								  "link"=>array(
												array("href"=>"templates/css/style.css","rel"=>"stylesheet","type"=>"text/css"),
												array("href"=>"http://fonts.googleapis.com/css?family=Merriweather+Sans:300,400,800,700","rel"=>"stylesheet","type"=>"text/css"),
												//array("href"=>"templates/navigation.css","rel"=>"stylesheet","type"=>"text/css")
												),
								  "meta"=>array(
												array('name'=>'description',"content"=>"Tri Hartanto Web Site")
												),
								  "title"=>array("separator"=>'||')
								  )
							);
$litedata['iprgm_id']					= '1';
$litedata['iprgm_name']					= 'app_site';
$litedata['iprgm_type']					= 'app';
$litedata['iprgm_order']				= '1';
$litedata['iprgm_nav_table']			= '';
$litedata['iprgm_html_head']			= $html_head;
$litedata['iprgm_template_id_class']	= '';
$litedata['iprgm_params']				= '';
$litedata['iprgm_configuration']		= json_encode($iprgm_config);
$litedata['iprgm_updatedate']			= '';
$litedata['iprgm_adddate']				= '';
$litedata['uprgm_id']					= '1';
$litedata['type_name']					= 'frontend';
//$litedata['type_index']					= 'index.php';
$litedata['tmpl_id']					= '';
$litedata['tmpl_name']					= '';
//$litedata['uprgm_name']					= (empty($_URLDecode['URLApp']))?'clickntrades':$_URLDecode['URLApp'];
$litedata['uprgm_name']					= array('home','contact','gallery','article','about');
$litedata['uprgm_title']				= array('home'=>'Welcome To Tri Hartanto Personal Website','contact'=>'Contact Tri Hartanto','article'=>'Tri Hartanto Article','about'=>'About Tri Hartanto & Website');
$litedata['uprgm_position']				= 'app';
$litedata['uprgm_location']				= '';#deprecated in app
$litedata['uprgm_iprgm_location']		= '';;#deprecated in app
$litedata['uprgm_nav_table']			= '';
$litedata['uprgm_params']				= '';
$litedata['uprgm_html_head']			= '';
$litedata['uprgm_status']				= array('home'=>true,'contact'=>true,'gallery'=>true,'article'=>true,'about'=>true);#can be set using 1 or 0
//$litedata['uprgm_default']				= ($litedata['uprgm_name']=='clickntrades')?true:false;
$litedata['uprgm_default']				= array('home'=>true);
$litedata['uprgm_order']				= '1';
$litedata['uprgm_template']				= 'custom';
$litedata['uprgm_template_id_class']	= '';
$litedata['uprgm_mvc_class']			= '';
$litedata['uprgm_mvc_method']			= '';
$litedata['uprgm_require_login']		= array('home'=>json_encode(array('logged','guest')),'contact'=>json_encode(array('logged','guest')),'article'=>json_encode(array('logged','guest')),'about'=>json_encode(array('logged','guest')));
$litedata['uprgm_configuration']		= json_encode($uprgm_config);
$litedata['uprgm_updatedate']			= '';
$litedata['uprgm_adddate']				= '';
?>