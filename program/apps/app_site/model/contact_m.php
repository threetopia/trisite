<?php
class Contact_M extends Systems
{
	public function contact_form($data=NULL)
	{
		/*
		 * Generate HTML Input
		 */
		$input['First Name'] = array('name'=>'firstname','type'=>'text','placeholder'=>'First Name','value'=>'','maxlength'=>20,'validation'=>array('required','striptags'));
		$input['Last Name']  = array('name'=>'lastname','type'=>'text','placeholder'=>'Last Name','value'=>'','maxlength'=>50,'validation'=>array('striptags'));
		$input['Email'] 	 = array('name'=>'email','type'=>'email','placeholder'=>'your@email.com','value'=>'','validation'=>array('required','striptags'));
		$input['Message'] 	 = array('name'=>'message','type'=>'textarea','value'=>'','validation'=>array('required','striptags'));
		// Captcha
		$input['Captcha']	 = '<img id="captcha" src="'.HTTP_LIBRARY_DIR.'/securimage/securimage_show.php"/>';
		$input[]			 = '<a href="#" style="color:black;" onclick="document.getElementById(\'captcha\').src = \''.HTTP_LIBRARY_DIR.'/securimage/securimage_show.php?\'+Math.random(); return false;">[Change Image]</a>';
		$input[] 			 = array('type'=>'text','name'=>'captcha_code','size'=>6,'maxlength'=>6,'value'=>'','validation'=>array('isvalid'=>($this->_LibSecurimage->check(((isset($_POST['captcha_code']))?$_POST['captcha_code']:''))),'invalid_message'=>'Captcha doesn\'t match with the captcha image'));
		// Captcha
		$input[] 			 = array('type'=>'submit','name'=>'send','class'=>'','onclick'=>'','value'=>'Send');
		
		$form = $this->_SysPubEngine->form_create(array('input'=>$input,'form'=>array('action'=>'','method'=>'post','enctype'=>'multipart/form-data'),'table'=>array('class'=>'login')));
		return $form;
	}
}
?>