<?php
(!defined('TRISYS'))?die('Opps direct access is not allowed'):NULL;
###########################################################
# Pixel Latte for trihartanto.com
# Template are made from pure PHP code
###########################################################
$html  = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
$html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
$html .= '	<head>';
$html .= '		'.$_SysEngine->render(array('program_type'=>'html_head')).'';
$html .= '	</head>';
$html .= '	<body>';
$html .= '		<div id="main">';
$html .= '			<div id="top">';
$html .= '				'.$_SysEngine->render(array('program_type'=>'module','position'=>'top')).'';
$html .= '			</div>';
$html .= '			<div id="left">';
$html .= '				'.$_SysEngine->render(array('program_type'=>'module','position'=>'left')).'';
$html .= '			</div>';
$html .= '			<div id="center">';
$html .= '				'.$this->render(array('program_type'=>'app','position'=>'app')).'';
$html .= '			</div>';
$html .= '			<div id="right">';
$html .= '				'.$_SysEngine->render(array('program_type'=>'module','position'=>'right')).'';
$html .= '			</div>';
$html .= '			<div id="footer">';
$html .= '				'.$_SysEngine->render(array('program_type'=>'module','position'=>'footer')).'';
$html .= '			</div>';
$html .= '		</div>';
$html .= '	</body>';	
$html .= '</html>';
echo($html);
?>