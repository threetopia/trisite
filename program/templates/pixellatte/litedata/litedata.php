<?php
(!defined('TRISYS'))?die('Opps direct access is not allowed'):NULL;
$litedata['tmpl_id'] 			= 1;
$litedata['tmpl_name'] 			= 'pixellatte';
$litedata['tmpl_title'] 		= 'Pixel Latte';
$litedata['tmpl_status'] 		= true;
$litedata['tmpl_default'] 		= true;
$litedata['tmpl_html_head'] 	= array(
										"link"=>array(
													  array("href"=>"manifest/css/style.css","rel"=>"stylesheet","type"=>"text/css"),
													  array("href"=>"manifest/css/navigation.css","rel"=>"stylesheet","type"=>"text/css"),
													  array("href"=>"manifest/css/app.css","rel"=>"stylesheet","type"=>"text/css"),
													  array("href"=>"manifest/css/copyright.css","rel"=>"stylesheet","type"=>"text/css"),
													  array("href"=>"manifest/css/breadcumb.css","rel"=>"stylesheet","type"=>"text/css")
													  )
										);
$litedata['type_name'] 			= 'frontend';
?>