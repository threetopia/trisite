<?php
class Main_Copyright_V extends Systems
{
	public function index()
	{
		$url = $this->createanchor(array('URLData'=>array('URLApp'=>'','URLLink'=>'','URLString'=>'','URLExtPath'=>''),'title'=>'trihartanto.com - threetopia'));
		return 'Copyright © '.date('Y',time()).' | '.$url.' | All rights reserved';
	}
}
?>