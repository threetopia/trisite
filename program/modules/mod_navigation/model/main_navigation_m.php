<?php
class Main_Navigation_M extends Systems
{
	private $NavData = array(
				  			 'Home'=>array('URLApp'=>'home','URLLink'=>'','URLExtPath'=>''),
							 'Article'=>array('URLApp'=>'article','URLLink'=>'','URLExtPath'=>''),
							 'Contact'=>array('URLApp'=>'contact','URLLink'=>'','URLExtPath'=>''),
							 'About'=>array('URLApp'=>'about','URLLink'=>'','URLExtPath'=>'')
							 );
	public function render()
	{
		//$html = $this->loadview(array('file'=>'shared/main_navigation.php','data'=>array(),'sourcedir'=>NULL,'grab'=>true));
		$html = $this->main_navigation_v->main_nav(array('NavData'=>$this->NavData));
		return $html;
	}
}
?>