<?php
class Main_Navigation_V extends Systems
{
	public function index($html=NULL)
	{
		return $html;
	}
	
	public function main_nav($data=NULL)
	{
		$html  = '';
		if(!empty($data['NavData']))
		{
			$loop = 0;
			$html .= '<ul>';
			foreach($data['NavData'] as $key=>$val)
			{
				$loop++;
				$active = ($this->_AppData[0]['uprgm_name']==strtolower($val['URLApp']))?'_active':'';
				$space = ($loop>1)?' space ':'';
				$url = $this->_SysPubEngine->createurl($val);
				$html .= '<li>';
				$html .= '	<a href="'.$url.'" title="'.$key.'">';
				$html .= '		<div class="menubox'.$active.$space.'">';
				$html .= '			<div class="'.(strtolower($key).'icon').'"><span></span></div>';
				$html .= '		</div>';
				$html .= '	</a>';
				$html .= '</li>';
			}
			$html .= '</ul>';
		}
		return $html;
	}
}
?>