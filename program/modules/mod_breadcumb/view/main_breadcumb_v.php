<?php
class Main_Breadcumb_V extends Systems
{
	public function index()
	{
		$URLDecode = $this->_URLDecode;
		$Path = array();
		unset($URLDecode['URLDomain'],$URLDecode['URLIndex']);
		$html  = ''.$this->createanchor(array('title'=>'Tri Hartanto','URLData'=>array('URLApp'=>'','URLLink'=>''))).'';
		$html .= (!empty($URLDecode['URLApp']))?' > '.$this->createanchor(array('title'=>ucfirst($URLDecode['URLApp']),'URLData'=>array('URLApp'=>$URLDecode['URLApp'],'URLLink'=>'','URLExtPath'=>'','URLString'))):NULL;
		$html .= (!empty($URLDecode['URLLink']))?' > '.$this->createanchor(array('title'=>ucfirst($URLDecode['URLLink']),'URLData'=>array('URLApp'=>$URLDecode['URLApp'],'URLLink'=>$URLDecode['URLLink'],'URLExtPath'=>'','URLString'))):NULL;
		if(!empty($URLDecode['URLExtPath']))
		{
			foreach($URLDecode['URLExtPath'] as $key=>$val)
			{
				(($key<=$key)?$Path[$key]=$val:'');
				$html .= (!empty($URLDecode['URLApp']))?' > '.$this->_SysPubEngine->createanchor(array('title'=>ucfirst($val),'URLData'=>array('URLApp'=>$URLDecode['URLApp'],'URLLink'=>$URLDecode['URLLink'],'URLExtPath'=>$Path,'URLString'=>NULL))):NULL;
			}
		}
		return $html;
	}
}
?>